package buu.informatics.s59160089.diceroller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var rollButton : Button = findViewById(R.id.roll_button)
        rollButton.setOnClickListener{
            //Toast.makeText(this,"eiei" , Toast.LENGTH_SHORT).show()
            rollDice()
        }

        findViewById<Button>(R.id.reset_button).setOnClickListener{
            findViewById<TextView>(R.id.result_text).text = "Hello World!"
            Toast.makeText(this,"Reset Clicked",Toast.LENGTH_SHORT).show()
        }




    }

    private fun rollDice() {
        val randomInt = Random().nextInt(6)+1
        val resultText = findViewById<TextView>(R.id.result_text)
        resultText.text = randomInt.toString()
        Toast.makeText(this , "Button Clicked" , Toast.LENGTH_SHORT).show()
    }

}
